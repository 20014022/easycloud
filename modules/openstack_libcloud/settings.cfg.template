[openstack]

# OpenStack Parameters
#
# API keys and default region definition. Keys can be obtained through the
# OpenStack Dashboard under Compute > Access & Security > Show Credentials

os_auth_url = 
os_username = 
os_password = 
# Project ID/Name can sometimes be referred as Tenant ID/Name
os_project_name = 
os_project_id = 
# Region (e.g., in Chameleon Cloud it can be RegionOne, CHI@TACC or CHI@UC)
os_region = 
# The client used to connect to the metering service; one of:
# - ceilometerclient: this is the Python bindings to Ceilometer API v2
# - gnocchi: this is the Gnocchi client [default]
#os_telemetry_metering = "ceilometerclient"
os_telemetry_metering = "gnocchi"

[re_demo]

# Reservation ID for cloning an instance with the Agent
# You can find this ID under Reservation > Leases > 
# <Your Lease Name> > Reservations > id
demo_reservation_id = 

[options]

# Time in seconds between measurements fetches
monitor_fetch_period = 60

# Granularity of the measurements in seconds
# Note: minimum value is 60 seconds
granularity = 60

# How many measurements are retrieved for a single metric each time
window_size = 5

# Minimum number of measurements (1-window_size) positive to a rule
# that must be positive in order to trigger an action
minimum_positive = 3

# Tells what measures sinks to enable
# This option takes a comma-separated list of labels, each of which is used to identify a specific "subsection" of this file, providing options specific to the asssociated measures sink.
# For instance:
#   measures_sinks = FOO, BAR
#   [measure_sink.FOO]
#   module = <fully qualified module name containing the class that implements FOO's measures sink>
#   class = <the name of the class that implements FOO's measures sink>
#   FOO_option1 = ...
#   FOO_option2 = ...
#   [measure_sink.BAR]
#   module = <fully qualified module name containing the class that implements BAR's measures sink>
#   class = <the name of the class that implements BAR's measures sink>
#   BAR_option1 = ...
#   BAR_option2 = ...
#   BAR_option3 = ...
# As shown in the above example, each subsection *must* contain at least two special options, namely:
# - module, whose value represents the fully qualified module name containing the class that implements a given measures sink, and
# - class, whose value represents the name of the class that implements a given measures sink.
measures_sinks = activemq, cassandra, file, kafka, rabbitmq, redis

# Settings related to file measures sinks

[measures_sink.activemq]

module = core.measures_sink.activemq
class = ActiveMQSink
# A comma-separated list of broker servers; for instance: broker1:port1, broker2:port2, broker3:port3
# Default: localhost:61613 
brokers = localhost:61613
# The optional login user
# Default: none
user = 
# The optional password of the login user
# Default: none
password = 
# The ActiveMQ destination (either a queue or a topic) where to publish measures
# collected from cloud platforms.
# Defaut: easycloud
destination = easycloud
# The ActiveMQ destination type; it can be either 'queue' or 'topic'.
# Defaut: topic
destination_type = topic

[measures_sink.cassandra]

module = core.measures_sink.cassandra
class = CassandraSink
# A comma-separated list of contact points to try connecting for cluster discovery; for instance:
# host1, host2:port2, host3
# Default: localhost
contact_points = localhost
# The server-side port to which Cassandra is listening
# Default: 9042
port = 9042
# The Cassandra keyspace where to collect measures.
# Default: easycloud
keyspace = easycloud
# Name of the Cassandra table (formerly known as column family), inside the
# above keyspace, where to collect measures.
# Note, the table name should not contain the keyspace qualifier
# Default: measures
table = measures

[measures_sink.file]

module = core.measures_sink.file
class = CsvFileSink
# The top-level path where to store CSV files.
# Default: <sys-temp-dir>/easycloud
basepath = /tmp/easycloud

[measures_sink.kafka]

module = core.measures_sink.kafka
class = KafkaSink
# A comma-separated list of broker servers for instance: broker1:port1, broker2:port2, broker3:port3
# Default: localhost:9092 
brokers = localhost:9092
# The Kafka topic where to publish measures collected from cloud platforms
# Defaut: easycloud
topic = easycloud

[measures_sink.rabbitmq]

module = core.measures_sink.rabbitmq
class = RabbitMQSink
# The URL to the RabbitMQ server (see `URLParameters` at https://pika.readthedocs.io/en/stable/modules/parameters.html#urlparameters)
# Default: amqp://localhost:5672
url = amqp://localhost:5672
# The RabbitMQ exchange to which sending messages (see https://www.rabbitmq.com/tutorials/amqp-concepts.html)
# Default: easycloud
exchange = easycloud

[measures_sink.redis]

module = core.measures_sink.redis
class = RedisSink
# The URL to the Redis server (see `redis.from_url()` at https://redis-py.readthedocs.io/en/latest/)
# Default: redis://localhost:6379/
url = redis://localhost:6379/
# The db parameter is the database number. You can manage multiple databases in Redis at once, and each is identified by an integer. The max number of databases is 16 by default
# Default: none
db = 0
